package com.example.api.ws.demo.controller;

import com.example.api.ws.demo.domain.model.Student;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/students")
public class StudentController {

    private static final List<Student> STUDENTS = Arrays.asList(
            new Student(1, "Lebron James"),
            new Student(2, "Steph Curry"),
            new Student(3, "James Harden")
    );

    @GetMapping(path = "/{studentId}")
    public ResponseEntity<Student> getStudent(@PathVariable Integer studentId) {

        log.debug("StudentController.getStudent(studentId[{}])", studentId);
        return new ResponseEntity<>(getStudentM(studentId), HttpStatus.OK);
    }

    private Student getStudentM(Integer id) {
        return STUDENTS
                .stream()
                .filter(student -> id.equals(student.getStudentId()))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Student " + id + " does not exist."));
    }
}
